""" Script to generate standard muscle plots.
- Muscle length versus joint angle
- Muscle moment arm versus joint angle
"""
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pybullet as p
import pybullet_data
from tqdm import tqdm
import time
import yaml

from farms_container import Container
import farms_pylog as pylog
from farms_models.utils import get_sdf_path, get_model_path
from farms_muscle.musculo_skeletal_system import MusculoSkeletalSystem
from farms_sdf.utils import construct_tree
from farms_sdf.sdf import ModelSDF


def get_link_name_from_index(model_id, link_index):
    """Get link name from pybullet link index

    Parameters
    ----------
    model_id : <int>
        Index of the model in pybullet scene
    link_index : <int>
        Index of the link in pybullet model

    Returns
    -------
    link_name : <str>
        Name of the link corresponding to the index
    """
    if link_index < 0:
        return p.getBodyInfo(model_id)[0].decode('UTF-8')
    return p.getJointInfo(model_id, link_index)[12].decode('UTF-8')


def get_joint_name_from_index(model_id, joint_index):
    """Get joint name from pybullet joint index

    Parameters
    ----------
    model_id : <int>
        Index of the model in pybullet scene
    joint_index : <int>
        Index of the joint in pybullet model

    Returns
    -------
    joint_name : <str>
        Name of the joint corresponding to the index
    """
    return p.getJointInfo(model_id, joint_index)[1].decode('UTF-8')


def convert_local_to_global(body_id, link_id, local_coordinate):
    """Convert local coordinate to global coordinates

    Parameters
    ----------
    body_id : <int>
        Index of the body in pybullet
    link_id : <int>
        Index of the link element in pybullet body
    local_coordinate : <list/tuple>
        Coordinate in the link frame

    Returns
    -------
    global_coordinate : <list/tuple>
        Coordinate in global frame
    """
    if link_id != -1:
        global_coordinate, _ = p.multiplyTransforms(
            *p.getLinkState(
                body_id, link_id, computeForwardKinematics=True
            )[4:6],
            local_coordinate, [0, 0, 0, 1]
        )
    else:
        global_coordinate, _ = p.multiplyTransforms(
            *p.getBasePositionAndOrientation(body_id),
            *(p.multiplyTransforms(
                *(p.invertTransform(
                    *p.getDynamicsInfo(body_id, -1)[3:5]
                )),
                local_coordinate, [0, 0, 0, 1]
            ))
        )
    return global_coordinate


def convert_global_to_local(body_id, link_id, global_coordinate):
    """Convert global coordinate to global coordinates

    Parameters
    ----------
    body_id : <int>
        Index of the body in pybullet
    link_id : <int>
        Index of the link element in pybullet body
    global_coordinate : <list/tuple>
        Coordinate in the link frame

    Returns
    -------
    local_coordinate : <list/tuple>
        Coordinate in local link frame
    """
    if link_id != -1:
        local_coordinate, _ = p.multiplyTransforms(
            *p.invertTransform(*p.getLinkState(
                body_id, link_id, computeForwardKinematics=True
            )[4:6]),
            global_coordinate, [0, 0, 0, 1]
        )
    else:
        local_coordinate, _ = p.multiplyTransforms(
            *p.invertTransform(*p.getBasePositionAndOrientation(body_id)),
            global_coordinate, [0, 0, 0, 1]
        )
    return local_coordinate


def convert_local_to_inertial(body_id, link_id, local_coordinate):
    """Convert local coordinate to local inertial coordinates

    Parameters
    ----------
    body_id : <int>
        Index of the body in pybullet
    link_id : <int>
        Index of the link element in pybullet body
    local_coordinate : <list/tuple>
        Coordinate in the link/urdf frame

    Returns
    -------
    inertial_coordinate : <list/tuple>
        Coordinate in local inertial frame
    """
    inertial_coordinate, _ = p.multiplyTransforms(
        *p.invertTransform(*p.getDynamicsInfo(body_id, link_id)[3:5]),
        local_coordinate, [0, 0, 0, 1]
    )
    return inertial_coordinate


def compute_moment_arm(joint_center, muscle_segment):
    """Compute the moment arm

    Parameters
    ----------
    joint_center : <ndarray>
        Global position of the joint center

    muscle_segment : <ndarray>
        Global positions of the path points of the muscle segment

    Returns
    -------
    out : <float>
        moment arm

    """
    #: Segment
    segment = muscle_segment[0] - muscle_segment[1]
    #: Comput moment arm
    moment_arm = np.cross(
        (joint_center - muscle_segment[0]),
        segment/np.linalg.norm(segment)
    )
    return moment_arm

#: SDF Loader


def load_sdf(sdf_path, **kwargs):
    """Load sdf into the world

    Parameters
    ----------
    sdf_path : <str>
        Path of the sdf model
    **kwargs : <dict>
        Extra args to pass to the sdf loader

    Returns
    -------
    model_id : <int>
        Pybullet id of the model in the physics simulation
    """
    rendering(0)
    model_id = p.loadSDF(sdf_path)[0]
    rendering(1)
    return model_id

#: Add Floor


def add_floor(floor_offset=[0.0, 0.0, -1.0], physics_id=0):
    p.setAdditionalSearchPath(pybullet_data.getDataPath())
    floor_id = p.loadURDF("plane.urdf", floor_offset)
    return floor_id

#: Disable default controllers in the model


def disable_default_controllers(model_id):
    """Disable the default controllers of pybullet

    Parameters
    ----------
    model_id: <int>
        Model id in the physics simulation
    """
    num_joints = p.getNumJoints(model_id)
    p.setJointMotorControlArray(
        model_id,
        np.arange(num_joints),
        p.VELOCITY_CONTROL,
        targetVelocities=np.zeros((num_joints,)),
        forces=np.zeros((num_joints,))
    )
    p.setJointMotorControlArray(
        model_id,
        np.arange(num_joints),
        p.POSITION_CONTROL,
        forces=np.zeros((num_joints,))
    )
    p.setJointMotorControlArray(
        model_id,
        np.arange(num_joints),
        p.TORQUE_CONTROL,
        forces=np.zeros((num_joints,))
    )

#: Enable or disable rendering


def rendering(render=1):
    """Enable/disable rendering"""
    p.configureDebugVisualizer(p.COV_ENABLE_RENDERING, render)
    p.configureDebugVisualizer(p.COV_ENABLE_GUI, render)

#: Connect to pybullet


def connect_pybullet(headless=True) -> int:
    """ Connect to pybullet simulation. """
    if not headless:
        return p.connect(p.GUI)
    return p.connect(p.DIRECT)

#: Setup PyBullet simulation evironment


def setup_simulation(physics_id, **kwargs):
    """ Setup simulation. """
    #: Set Gravity
    gravity = kwargs.pop("gravity", [0.0, 0.0, -9.81])
    p.setGravity(gravity[0], gravity[1], gravity[2], physics_id)
    #: Physics engine parameters
    p.setRealTimeSimulation(0, physics_id)
    p.setPhysicsEngineParameter(
        fixedTimeStep=kwargs.pop('time_step', 0.001),
        numSolverIterations=kwargs.pop('solver_iterations', 100),
        numSubSteps=1,
        solverResidualThreshold=1e-10,
        erp=kwargs.pop('erp', 0.1),
        contactERP=kwargs.pop('contact_erp', 0.0),
        frictionERP=kwargs.pop('friction_erp', 0.0),
    )


def generate_joint_id_name_dict(model_id):
    """ Generate a dictionary to map joint id to name """
    return {
        p.getJointInfo(model_id, n)[1].decode('UTF-8'):
        p.getJointInfo(model_id, n)[0]
        for n in range(p.getNumJoints(model_id))
    }


def get_joint_range(model_id, joint_index):
    """ Get the range of joint. """
    joint_name_to_index = generate_joint_id_name_dict(model_id)
    return p.getJointInfo(
        model_id, joint_index
    )[8:10]


def get_length_versus_joint_angle(
        model_id, container, muscle_system, muscle_name, joint_index,
        **kwargs
):
    """ Get length versus joint angle relationship. """
    joint_resolution = kwargs.pop('joint_resolution', 0.01)  # : radians
    joint_range = get_joint_range(model_id, joint_index)
    joint_angles = np.arange(joint_range[0], joint_range[1], joint_resolution)
    lmtu = container.muscles.parameters.get_parameter(
        'lmtu_{}'.format(muscle_name)
    )
    muscle_lengths = np.zeros(np.shape(joint_angles))
    original_angle = p.getJointState(model_id, joint_index)[0]
    for j, val in enumerate(joint_angles):
        p.resetJointState(
            model_id, joint_index, targetValue=val
        )
        muscle_system.step()
        muscle_lengths[j] = lmtu.value
    #: Reset joint to original angle
    p.resetJointState(
        model_id, joint_index, targetValue=original_angle
    )
    return joint_angles, muscle_lengths


def get_joint_moment_arm(
        model_id, container, muscle_system, muscle_name, joint_index,
        **kwargs
):
    """ Compute the moment arm. """
    sdf_model = kwargs.pop("sdf_model", None)
    sdf_model_tree = construct_tree(sdf_model)
    joint_name = get_joint_name_from_index(model_id, joint_index)
    joint_resolution = kwargs.pop('joint_resolution', 0.01)  # : radians
    joint_center = np.asarray(
        p.getLinkState(
            model_id, joint_index, computeForwardKinematics=True
        )[4]
    )
    joint_axis = p.getJointInfo(model_id, joint_index)[13]
    joint_range = get_joint_range(model_id, joint_index)
    joint_angles = np.arange(joint_range[0], joint_range[1], joint_resolution)
    moment_arm = np.zeros((len(joint_angles),))
    original_angle = p.getJointState(model_id, joint_index)[0]
    export_plot = kwargs.pop("export_plot", True)
    #: Get the coordinates influenced by the joint
    local_waypoints = muscle_system.muscles[muscle_name].local_waypoints
    muscle_segments = []
    for j in range(len(local_waypoints)-1):
        origin_link = get_link_name_from_index(model_id, local_waypoints[j][0])
        insertion_link = get_link_name_from_index(
            model_id, local_waypoints[j+1][0])
        subtree = sdf_model_tree.subtree(origin_link)
        intermediate_links = [node for node in subtree.rsearch(insertion_link)]
        intermediate_joints = [
            subtree.get_node(link).data.joint
            for link in intermediate_links
        ][:-1]
        muscle_segments.append(
            [origin_link, insertion_link, intermediate_joints]
        )
    segment_indices = None
    for j, seg in enumerate(muscle_segments):
        if joint_name in seg[2]:
            segment_indices = (j, j+1)
    for j, val in enumerate(joint_angles):
        p.resetJointState(model_id, joint_index, targetValue=val)
        muscle_system.step()
        global_waypoints = muscle_system.muscles[muscle_name].global_waypoints
        moment_arm[j] = compute_moment_arm(
            joint_center,
            global_waypoints[segment_indices[0]:segment_indices[1]+1]
        )[np.nonzero(joint_axis)[0][0]]
    #: Reset joint to original angle
    p.resetJointState(
        model_id, joint_index, targetValue=original_angle
    )
    #: Export the plots
    return joint_angles, moment_arm


def main(**kwargs):
    """ Main function """
    #: Connect to pybullet
    physics_id = connect_pybullet()
    #: Add floor
    add_floor(floor_offset=[0.0, 0.0, -2.0], physics_id=physics_id)
    #: Model
    model_name = kwargs.pop('model', 'simple_arm')
    model_version = kwargs.pop('version', '0')
    #: Load sdf model
    sdf_path = get_sdf_path(name=model_name, version=model_version)
    sdf_model = ModelSDF.read(sdf_path)[0]
    model_id = load_sdf(sdf_path)
    #: Setup simulation
    setup_simulation(physics_id)
    #: Setup container
    container = Container()
    #: Setup muscle simulation
    model_path = get_model_path(name=model_name, version=model_version)
    muscle_path = "./simple_arm/optimization_two_muscle_models/config/muscles.yaml"
    pylog.debug("Muscle path : {}".format(muscle_path))
    muscle_system = MusculoSkeletalSystem(container, muscle_path)
    #: Initialize
    container.initialize()
    joint_name_to_index = generate_joint_id_name_dict(model_id)
    #: Generate data
    muscles_joints = kwargs.pop('muscles_joints', [])
    #: Loop the muscle-joint list
    folder = "./muscle_tendon_length"
    for muscle_joint in tqdm(muscles_joints):
        muscle = "{}".format(muscle_joint[0])
        joint = "{}".format(muscle_joint[1])
        pylog.debug("{} - {}".format(muscle, joint))
        joint_angles, muscle_lengths = get_length_versus_joint_angle(
            model_id, container, muscle_system, muscle,
            joint_name_to_index[joint]
        )
        file_name = "{}_{}_muscle_tendon_length.h5".format(
            muscle_joint[0], muscle_joint[1]
        )
        data = pd.DataFrame(
            {
                muscle_joint[1]: joint_angles,
                muscle_joint[0]: muscle_lengths
            }
        )
        data.to_hdf(
            os.path.join(folder, file_name),
            "{}_{}".format(muscle_joint[0], muscle_joint[1]),
            mode='w'
        )
    folder = "./moment_arm"
    for muscle_joint in tqdm(muscles_joints):
        muscle = "{}".format(muscle_joint[0])
        joint = "{}".format(muscle_joint[1])
        pylog.debug("{} - {}".format(muscle, joint))
        joint_angles, moment_arms = get_joint_moment_arm(
            model_id, container, muscle_system, muscle,
            joint_name_to_index[joint], sdf_model=sdf_model
        )
        file_name = "{}_{}_moment_arm.h5".format(
            muscle_joint[0], muscle_joint[1]
        )
        data = pd.DataFrame(
            {
                muscle_joint[1]: joint_angles,
                muscle_joint[0]: moment_arms
            }
        )
        data.to_hdf(
            os.path.join(folder, file_name),
            "{}_{}".format(muscle_joint[0], muscle_joint[1]),
            mode='w'
        )
    # plt.plot(joint_angles, muscle_arms)
    # plt.grid(True)
    # plt.show()
    # plt.plot(joint_angles, muscle_lengths)
    # plt.grid(True)
    # plt.show()


if __name__ == '__main__':
    with open("./mouse_hind_limb_muscles_joints.yaml", "r") as stream:
        muscles_joints = yaml.load(stream, yaml.SafeLoader)
    main(muscles_joints=[["flexor", "joint"], ["extensor", "joint"]])
