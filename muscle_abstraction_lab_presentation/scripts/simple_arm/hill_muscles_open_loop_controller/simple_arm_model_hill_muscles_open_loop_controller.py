""" Test simple arm model with hill muscles in pybullet. """

from farms_models.utils import get_sdf_path
from bullet_simulation import BulletSimulation
from IPython import embed
import pybullet as p
import pybullet_data
import pandas as pd
import numpy as np
import time
import farms_pylog as pylog
import numpy as np
import os
from farms_sdf.units import SimulationUnitScaling
from farms_container import Container
from farms_network.neural_system import NeuralSystem
import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt
from farms_muscle.bullet_interface import BulletInterface
pylog.set_level('debug')

#: Generate four neuron leaky integrator oscillator network
def generate_network_graph(name_prefix='neuron', **kwargs):
    """ Generate a neuron graph network using the weight matrix """
    #: Weight matrix
    weight_matrix = np.asarray(
        kwargs.pop('weight_matrix', np.identity(4))
    )
    #: Number of neurons
    n_neurons = len(weight_matrix)
    #: Neuron properties
    neurons_tau = kwargs.pop('tau', np.ones((n_neurons,))*0.1)
    neurons_D = kwargs.pop('D', np.ones((n_neurons,))*2)
    neurons_b = kwargs.pop('b', np.ones((n_neurons,))*3)

    #: Define a network graph
    network = nx.DiGraph()
    neuron_names = [
        "{}_{}".format(name_prefix, n) for n in range(n_neurons)
    ]
    for j, osc in enumerate(neuron_names):
        network.add_node(
            osc, model="leaky",
            tau=neurons_tau[j],
            D=neurons_D[j],
            bias=neurons_b[j],
        )
    #: Connect
    for i, j in zip(*np.nonzero(weight_matrix)):
        weight = weight_matrix[i, j]
        network.add_edge(
            neuron_names[i],
            neuron_names[j],
            weight=weight,
        )
    return network


class SimpleArmSimulation(BulletSimulation):
    """SimpleArm Simulation Class
    """

    def __init__(self, container, sim_options):
        super(SimpleArmSimulation, self).__init__(
            container, SimulationUnitScaling(), **sim_options
        )
        muscle_inputs = container.muscles.activations
        neural_outputs = container.neural.outputs
        neural_inputs = container.neural.inputs
        self.neural_drive = {}
        self.neural_inputs = {}
        self.muscle_excitation = {}
        self.neural_activations = {}
        for muscle in self.muscles.muscles.keys():
            self.muscle_excitation[muscle] = muscle_inputs.get_parameter(
                'stim_{}'.format(muscle)
            )
        for neuron in self.controller.network.neurons.keys():
            self.neural_activations[neuron] = neural_outputs.get_parameter(
                'nout_{}'.format(neuron)
            )
            self.neural_inputs[neuron] = neural_inputs.get_parameter(
                'ext_in_{}'.format(neuron)
            )
        self.neural_drive = p.addUserDebugParameter(
            "external_drive", 0., 1, 0.05
        )

    def controller_to_actuator(self):
        """ Implementation of abstractmethod. """
        drive = p.readUserDebugParameter(self.neural_drive)
        for neuron in self.controller.network.neurons.keys():
            self.neural_inputs[neuron].value = drive
        self.muscle_excitation['flexor'].value = self.neural_activations[
            'neuron_0'].value
        self.muscle_excitation['extensor'].value = self.neural_activations[
            'neuron_1'].value

    def feedback_to_controller(self):
        """ Implementation of abstractmethod. """
        pass

    def update_parameters(self):
        """ Implementation of abstractmethod. """
        pass

    def optimization_check(self):
        """ Implementation of abstractmethod. """
        pass


def main():
    """ Main """

    #: Create the controller
    weight_matrix = np.asarray(
        [[0., -5., -5., 0.],
         [-5., 0., 0., -5.],
         [5., -5., 0., 0.],
         [-5., 5., 0., 0.]]
    )
    tau = np.asarray([0.02, 0.02, 0.1, 0.1])
    D = np.ones((4))*2
    b = np.asarray([3.0, 3.0, -3.0, -3.0])
    network = generate_network_graph(
        neuron_prefix='neuron', weight_matrix=weight_matrix.T, tau=tau,
        D=D, b=b
    )
    #: Location to save the network
    controller_export_path = "./config/auto_four_neuron_oscillator.graphml"
    nx.write_graphml(network, controller_export_path)

    sim_options = {"headless": False,
                   "model": get_sdf_path(name="simple_arm", version="0"),
                   "model_offset": [0., 0., 0.],
                   "floor_offset": [0, 0., -3],
                   "run_time": 5.,
                   "time_step": 0.001,
                   "planar": False,
                   "muscles": "config/muscles.yaml",
                   "controller": "config/auto_four_neuron_oscillator.graphml",
                   "track": False,
                   "slow_down": True,
                   "sleep_time": 0.001,
                   "base_link": "base",
                   "pose": "config/pose.yaml",
                   }
    container = Container(max_iterations=int(
        sim_options['run_time']/sim_options['time_step']))
    animal = SimpleArmSimulation(container, sim_options)
    animal.run()
    container.dump(overwrite=True)
    time_vector = np.arange(
        0.0, sim_options["run_time"], sim_options["time_step"]
    )
    #: Plot
    #: Show network
    animal.controller.visualize_network(
        edge_labels=True, edge_attribute='weight'
    )
    forces = pd.read_hdf('./Results/muscles/forces.h5')
    lengths = pd.read_hdf('./Results/muscles/parameters.h5')
    dstates = pd.read_hdf('./Results/muscles/dstates.h5')
    neural_outputs = pd.read_hdf('./Results/neural/outputs.h5')
    neural_states = pd.read_hdf('./Results/neural/states.h5')
    plt.figure()
    plt.title("Muscle tendon forces")
    plt.plot(time_vector, forces)
    plt.xlabel("Time [s]")
    plt.ylabel("Force [N]")
    plt.legend(forces.keys())
    plt.grid(True)
    plt.figure()
    plt.title("Muscle tendon lengths")
    plt.plot(time_vector, lengths)
    plt.xlabel("Time [s]")
    plt.ylabel("Length [m]")
    plt.legend(lengths.keys())
    plt.grid(True)
    plt.figure()
    plt.title("Muscle state derivatives")
    plt.plot(time_vector, dstates)
    plt.xlabel("Time [s]")
    plt.legend(dstates.keys())
    plt.grid(True)
    plt.figure()
    plt.title("Neural outputs")
    plt.plot(time_vector, neural_outputs)
    plt.xlabel("Time [s]")
    plt.ylabel("Output [0-1]")
    plt.legend(neural_outputs.keys())
    plt.grid(True)
    plt.figure()
    plt.title("Neuron states")
    plt.plot(time_vector, neural_states)
    plt.xlabel("Time [s]")
    plt.ylabel("[]")
    plt.legend(neural_states.keys())
    plt.grid(True)
    plt.show()


if __name__ == '__main__':
    main()
