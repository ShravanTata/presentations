""" Demo. """

from datetime import datetime
from pathlib import Path
import logging
from farms_models.utils import get_sdf_path
from animals.generic.bullet_simulation import BulletSimulation
from IPython import embed
import pybullet as p
import pybullet_data
import pandas as pd
import numpy as np
import time
import farms_pylog as pylog
import numpy as np
import os
from dataclasses import dataclass
from farms_sdf.units import SimulationUnitScaling
from farms_container import Container
import pandas as pd
import matplotlib.pyplot as plt
from farms_muscle.bullet_interface import BulletInterface
import networkx as nx

plt.rcParams.update({'font.size': 21})
pylog.set_level("error")

############### CONTROLLER ###############
#: Generate four neuron leaky integrator oscillator network
def generate_network_graph(name_prefix='neuron', **kwargs):
    """ Generate a neuron graph network using the weight matrix """
    #: Weight matrix
    weight_matrix = np.asarray(
        kwargs.pop('weight_matrix', np.identity(4))
    )
    #: Number of neurons
    n_neurons = len(weight_matrix)
    #: Neuron properties
    neurons_tau = kwargs.pop('tau', np.ones((n_neurons,))*0.1)
    neurons_D = kwargs.pop('D', np.ones((n_neurons,))*2)
    neurons_b = kwargs.pop('b', np.ones((n_neurons,))*3)

    #: Define a network graph
    network = nx.DiGraph()
    neuron_names = [
        "{}_{}".format(name_prefix, n) for n in range(n_neurons)
    ]
    for j, osc in enumerate(neuron_names):
        network.add_node(
            osc, model="leaky",
            tau=neurons_tau[j],
            D=neurons_D[j],
            bias=neurons_b[j],
        )
    #: Connect
    for i, j in zip(*np.nonzero(weight_matrix)):
        weight = weight_matrix[i, j]
        network.add_edge(
            neuron_names[i],
            neuron_names[j],
            weight=weight,
        )
    return network


########## MUSCLE MODEL ##########
@dataclass
class EkebergMuscleParameters:
    """ EkebergMuscleParameters """
    alpha : float = 0.0
    beta : float = 10.0
    gamma : float = 10.0
    delta : float = 10.0
    flexion : float = 0.0
    extension : float = 0.0
    rest_position : float = 1.4

def muscle(model, muscle_parameters=EkebergMuscleParameters()):
    """ Muscle. """
    alpha = muscle_parameters.alpha
    beta = muscle_parameters.beta
    gamma = muscle_parameters.gamma
    delta = muscle_parameters.delta
    flexion = muscle_parameters.flexion
    extension = muscle_parameters.extension
    rest_position = muscle_parameters.rest_position
    (position, velocity) = p.getJointState(model, 0)[0:2]
    passive_spring = beta*gamma*(rest_position-position)
    passive_damper = -1*delta*(velocity)
    active_muscle = alpha*(flexion-extension) + beta*(
        flexion+extension)*(rest_position-position)
    return (passive_spring + passive_damper + active_muscle)

class SimpleArmHillMuscleSimulation(BulletSimulation):
    """SimpleArm with hill muscle Simulation Class
    """

    def __init__(self, container, sim_options):
        super(SimpleArmHillMuscleSimulation, self).__init__(
            container, SimulationUnitScaling(), **sim_options
        )
        muscle_inputs = container.muscles.activations
        neural_outputs = container.neural.outputs
        neural_inputs = container.neural.inputs
        self.neural_inputs = {}
        self.muscle_excitation = {}
        self.neural_activations = {}
        for muscle in self.muscles.muscles.keys():
            self.muscle_excitation[muscle] = muscle_inputs.get_parameter(
                'stim_{}'.format(muscle)
            )
        for neuron in self.controller.network.neurons.keys():
            self.neural_activations[neuron] = neural_outputs.get_parameter(
                'nout_{}'.format(neuron)
            )
            self.neural_inputs[neuron] = neural_inputs.get_parameter(
                'ext_in_{}'.format(neuron)
            )

    def controller_to_actuator(self):
        """ Implementation of abstractmethod. """
        self.muscle_excitation['flexor'].value = self.neural_activations[
            'neuron_0'].value
        self.muscle_excitation['extensor'].value = self.neural_activations[
            'neuron_1'].value

    def feedback_to_controller(self):
        """ Implementation of abstractmethod. """
        pass

    def update_parameters(self):
        """ Implementation of abstractmethod. """
        pass

    def optimization_check(self):
        """ Implementation of abstractmethod. """
        pass

class SimpleArmEkebergMuscleSimulation(BulletSimulation):
    """SimpleArm with ekeberg muscle Simulation Class """

    def __init__(self, container, sim_options):
        super(SimpleArmEkebergMuscleSimulation, self).__init__(
            container, SimulationUnitScaling(), **sim_options
        )
        self.muscle_parameters = EkebergMuscleParameters()

        neural_outputs = container.neural.outputs
        neural_inputs = container.neural.inputs
        self.neural_inputs = {}
        self.neural_activations = {}
        for neuron in self.controller.network.neurons.keys():
            self.neural_activations[neuron] = neural_outputs.get_parameter(
                'nout_{}'.format(neuron)
            )
            self.neural_inputs[neuron] = neural_inputs.get_parameter(
                'ext_in_{}'.format(neuron)
            )

    def controller_to_actuator(self):
        """ Implementation of abstractmethod. """
        self.muscle_parameters.alpha = 10
        self.muscle_parameters.flexion = self.neural_activations[
            'neuron_0'].value
        self.muscle_parameters.extension = self.neural_activations[
            'neuron_1'].value

        torque = muscle(self.animal, self.muscle_parameters)
        p.setJointMotorControl2(
            self.animal, 0, p.TORQUE_CONTROL, force=torque
        )

    def feedback_to_controller(self):
        """ Implementation of abstractmethod. """
        pass

    def update_parameters(self, parameters):
        """ Implementation of abstractmethod. """
        self.muscle_parameters.beta = parameters[0]
        self.muscle_parameters.gamma = parameters[1]
        self.muscle_parameters.delta = parameters[2]
        self.muscle_parameters.rest_position = parameters[3]

    def optimization_check(self):
        """ Implementation of abstractmethod. """
        pass

############### GENERAL SIMULATION RUN ###############
def main():

    #: Create the controller
    weight_matrix = np.asarray(
        [[0., -5., -5., 0.],
         [-5., 0., 0., -5.],
         [5., -5., 0., 0.],
         [-5., 5., 0., 0.]]
    )
    tau = np.asarray([0.02, 0.02, 0.1, 0.1])
    D = np.ones((4))*2
    b = np.asarray([3.0, 3.0, -3.0, -3.0])
    network = generate_network_graph(
        neuron_prefix='neuron', weight_matrix=weight_matrix.T, tau=tau,
        D=D, b=b
    )
    #: Location to save the network
    controller_export_path = "./config/auto_four_neuron_oscillator.graphml"
    nx.write_graphml(network, controller_export_path)
    sim_time = 3.
    sim_time_step = 0.001
    sim_options_arm_hill = {
        "headless": False,
        "model": get_sdf_path("simple_arm", "0"),
        "model_offset": [0., -1., 0.],
        "floor_offset": [0, 0., -3],
        "run_time": sim_time,
        "time_step": sim_time_step,
        "planar": False,
        "muscles": "config/muscles.yaml",
        "controller": "config/auto_four_neuron_oscillator.graphml",
        "track": False,
        "slow_down": False,
        "sleep_time": 0.001,
        "base_link": "base",
        "pose": "config/pose.yaml",
    }
    container_arm_hill = Container(max_iterations=int(
        sim_options_arm_hill['run_time']/sim_options_arm_hill['time_step']))
    arm_hill = SimpleArmHillMuscleSimulation(
        container_arm_hill, sim_options_arm_hill
    )
    sim_options_arm_ekeberg = {
        "headless": False,
        "model": get_sdf_path("simple_arm", "0"),
        "model_offset": [0., 1., 0.],
        "floor_offset": [0, 0., -3],
        "run_time": sim_time,
        "time_step": sim_time_step,
        "controller": "config/auto_four_neuron_oscillator.graphml",
        "planar": False,
        "track": False,
        "slow_down": False,
        "sleep_time": 0.001,
        "base_link": "base",
        "pose": "config/pose.yaml",
    }
    container_arm_ekeberg = Container(max_iterations=int(
        sim_time/sim_time_step))
    arm_ekeberg = SimpleArmEkebergMuscleSimulation(
        container_arm_ekeberg, sim_options_arm_ekeberg
    )
    arm_ekeberg.update_parameters(
        [9.70131561, 1.9409482,  2.0871692, 1.54018641]
    )
    time_steps = np.linspace(
        0,
        sim_time,
        int(sim_time/sim_time_step)
    )
    for t in time_steps:
        arm_hill.step()
        arm_ekeberg.step()
    hill_joint_angles = container_arm_hill.physics.joint_positions.log
    ekeberg_joint_angles = container_arm_ekeberg.physics.joint_positions.log
    
    arm_hill.controller.visualize_network(
        edge_labels=True, edge_attribute='weight'
    )

    plt.figure()
    plt.plot(time_steps, hill_joint_angles, linewidth=4)
    plt.plot(time_steps, ekeberg_joint_angles, linewidth=4)
    plt.grid(True)
    plt.legend(("Hill muscle joint angle", "Ekeberg muscle joint angle"))
    plt.title("Hill and Ekeberg model passive response")
    plt.ylabel("Joint angle [rad]")
    plt.xlabel("Time [s]")
    mid_point = (np.min(hill_joint_angles)+np.max(hill_joint_angles))/2
    plt.show()    

if __name__ == '__main__':
    main()
