""" plot muscle properties. """

import matplotlib.pyplot as plt
import pandas as pd

plt.rcParams.update({'font.size': 21})

flex = pd.read_hdf("./moment_arm/flexor_joint_moment_arm.h5")
ext = pd.read_hdf("./moment_arm/extensor_joint_moment_arm.h5")
plt.plot(flex["joint"], flex["flexor"], linewidth=4)
plt.plot(ext["joint"], ext["extensor"], linewidth=4)
plt.title("Flexor-Extensor Moment arm v/s Joint Angle")
plt.legend(("Flexor", "Extensor"))
plt.xlabel("Joint angle [rad]")
plt.ylabel("Moment arm [m]")
plt.grid(True)
plt.show()
# plt.savefig("../figures/analysis_moment_arm.png")
flex = pd.read_hdf(
    "./muscle_tendon_length/flexor_joint_muscle_tendon_length.h5")
ext = pd.read_hdf(
    "./muscle_tendon_length/extensor_joint_muscle_tendon_length.h5")
plt.plot(flex["joint"], flex["flexor"], linewidth=4)
plt.plot(ext["joint"], ext["extensor"], linewidth=4)
plt.legend(("Flexor", "Extensor"))
plt.title("Flexor-Extensor Length v/s Joint Angle")
plt.xlabel("Joint angle [rad]")
plt.ylabel("Muscle length [m]")
plt.grid(True)
plt.show()
# plt.savefig("../figures/analysis_muscle_length.png")
