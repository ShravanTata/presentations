#+TITLE: PhD Thesis Plan
#+SUBTITLE: Timeline, Planning and Goal
#+AUTHOR: Shravan Tata
#+EMAIL: Shravan.tataramalingasetty@epfl.ch
#+DATE: <2021-01-20 Wed>
#+STARTUP: beamer
#+DESCRIPTION: Plan the final phase of the PhD
#+LANGUAGE: en
#+OPTIONS: H:2 num:nil ^:{} toc:nil
#+LaTeX_CLASS_OPTIONS: [small, presentation, aspectratio=169]
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing,patterns}
#+LATEX_HEADER: \usetikzlibrary{positioning}
#+LATEX_HEADER: \usepackage[backend=bibtex]{biblatex}
#+LATEX_HEADER: \bibliography{bibliography.bib}
#+BIBILOGRAPHY: bibliography.bib
#+BEAMER_THEME: metropolis
#+BEAMER_COLOR_THEME: beaver
#+BEAMER_FONT_THEME: structuresmallcapsserif
#+BEAMER_HEADER: \graphicspath{{./figures/}}
#+EXCLUDE_TAGS: noexport
#+PROPERTY:  header-args :eval no

* Overview                                                         :noexport:
* Musculoskeletal models                                           :noexport:
** Mouse
*** Overview                                                          :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :END:
**** Skeletal System                                                :B_block:
     :PROPERTIES:
     :BEAMER_env: block
     :END:
     - [X] Fully articulated muscle model
     - [-] Skin for interial property estimation
**** Musculature                                                    :B_block:
     :PROPERTIES:
     :BEAMER_env: block
     :END:
     - [X] Full hindlimb musculature
     - [X] Preliminary forelimb musculature
     - [-] Spinal muscles
*** Figure                                            :B_ignoreheading:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
    #+ATTR_LATEX: :scale 0.5 :center :options trim=2cm 2cm 2cm 2cm
    #+CAPTION: Mouse model
    [[file:media/figures/mouse.png]]

** Drosophila Melanogaster
*** Overview                                                          :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :END:
**** Skeletal System
     - [X] CT Scan and fully articulate model

**** Musculature
     - [-] Preliminary identification muscle attachments for limbs

*** Figure                                            :B_ignoreheading:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
    #+ATTR_LATEX: :scale 0.75 :center nil
    #+CAPTION: fly model
    [[file:media/figures/fly.png]]

** Human
*** Overview                                                          :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :END:

**** Skeletal System
     - [X] Several standard models available. Ported two models to *FARMS*.

**** Musculature
     - [X] Full body muscle system available and ported to *FARMS*

*** Figure                                            :B_ignoreheading:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
    #+ATTR_LATEX: :scale 0.15 :center nil :options trim=2cm 2cm 2cm 2cm
    #+CAPTION: Human model
    [[file:media/figures/human.png]]

** Guinea Fowl
*** Overview                                                          :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :END:
**** Skeletal System
     - [X] Single hindlimb model available. Mirrored to developed the full model in *FARMS*

**** Musculature
     - [X] Same paper has identified the muscle. Ported to *FARMS*

*** Figure                                            :B_ignoreheading:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
    #+ATTR_LATEX: :scale 0.1 :center nil
    #+CAPTION: Guinea model
    [[file:media/figures/guinea.png]]

** Cat

*** Skeketal System
    - [X] Obtained skeletal system from an online resource
    - [-] Need to articulate the model and port to *FARMS*

*** Musculature
    - [X] Literature data available for hindlimb muscles
    - [-] Sergey from Ilya Rybak's team will handle this part
* Controllers                                                      :noexport:
** Quadruped controllers from Ilya Rybak
   - [X] Implemented in *FARMS*
   - [X] Tested with Webots model for quadruped locomotion
   - [X] Preliminary closed loop models also available
   - [-] Currently testing with full *FARMS*
** Simple phasic controllers from AOI
   - [X] Re-implemented the original controller
   - [X] Extended to more complex regimes
   - Useful to explore both open loop and sensory feedback
   - Easily extendable to multiple animal models
** Classic BioRob CPG networks
   - [X] Full implementation available
   - [X] Tested the controllers for sagital plan locomotion with Guinea fowl
   - [-] To be integrated with AOI controller
** Sensory controller from Ekeberg
   - [X] Preliminary studies done and tested during CMC
   - [-] Need to extend to quadruped
   - [-] Explore integration with CPG models

* Papers
** List of Papers
   #+ATTR_BEAMER: :overlay <+->
   - *Spatiotemporal Maps of Proprioceptive Inputs to the Cervical*
     *Spinal Cord During Three- Dimensional Reaching and Grasping*
   - *NeuroMechFly : A neuromechanical simulation of Drosophila Melanogaster*
   - /A Biomechanical model template of mice to study motor control./
   - /FARMS : Framework for Animal-Robot Modeling and Simulation/
   - /Cat and Mouse : Comparing the role two sensory signals in/
     /generating stable locomotion using predictive simulations/
   - /Sensory driven control to study asymmetries in mouse hindlimb/
   - /Guinea Fowl : Sensory driven simulation model to generate biped gaits/
** A Biomechanical model template of mice to study motor control.  :noexport:
** FARMS : Framework for Animal-Robot Modeling and Simulation      :noexport:
** NeuroMechFly : A neuromechanical simulation of Drosophila Melanogaster :noexport:
* Thesis
** Committee
*** External                                                  :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
   - *John Hutchinson*
   - *Orjan Ekeberg*
   - *Shinya Aoi*
   - Boris
   - /Monica Dailey/
   - /Anne koelewijn/
   - /Friedel De Groote/
   - /Ole Khien/ (Outside neuromechanical)
*** Internal                                                  :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
   - /Mackenzie Mathis/
** Report
   #+ATTR_BEAMER: :overlay <+->
   - Introduction
   - FARMS
   - Modeling
     + Biomechanical models of mice, guinea-fowl
   - Neural Controllers
     + Different CPG's models
   - Feedforward
     - CPG driven mouse locomotion (Collaboration with Matteo)
     - Mouse open loop locomotion model
     - Drosophila locomotion model
     - Guinea fowl model
   - Sensory
     - Rhesus macaque sensory map
     - Mouse hindlimb model with Simon
     - Ekeberg mouse model
** Outline
*** Summary
    "Neuromechanical models play an important role in investigating
    neural circuitry involved in motor control, specifically
    locomotion. One of the contributions of the PhD was to develop
    detailed 3D musculoskeletal models primarily of mice and
    drosophila melnogaster. Then, based on the musculoskeletal models,
    we explored several neural circuits; both feed-forward and sensory
    driven models to develop predictive simulations and study
    overground locomotor behaviors. Along with it, an open-source
    neuromechanical simulation framework for simulating and studying
    animal movements was developed."
* Timeline                                                         :noexport:
** Paper deadlines
   #+ATTR_BEAMER: :overlay <+->
   - /A Biomechanical model template of mice to study motor control./ (*May 2021*)
   - /Cat and Mouse : Comparing the role two sensory signals in/
     /generating stable locomotion using predictive simulations/ (*August
     2021*)
   - /Sensory driven control to study asymmetries in mouse hindlimb/ (*August 2021*)
   - /farms_network/, /farms_muscle/ (*July 2021*) (Open source)
** Milestones
*** End of October with +6 months (Public defense)
*** End of September (Private defense)
*** Mid of August (Submission)
