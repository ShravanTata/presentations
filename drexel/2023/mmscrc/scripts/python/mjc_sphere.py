#!/usr/bin/env python

""" Mujoco script to run a free falling sphere """

import mujoco
import tqdm
from mujoco import viewer

xml = """
<mujoco>
     <asset>
        <texture name="grid" type="2d" builtin="checker" rgb1=".1 .2 .3"
         rgb2=".2 .3 .4" width="300" height="300"/>
        <texture name="mouse_skin" type="2d" file="/Users/tatarama/data/mouse/skin/fur_diffuse.png"/>
        <material name="mouse_skin" texture="mouse_skin" />
        <material name="grid" texture="grid" texrepeat="8 8" reflectance=".2"/>
        <mesh name="mesh_mouse_sphere" file="/Users/tatarama/data/mouse/skin/mouse_sphere.obj" scale="1 1 1"/>
    </asset>

    <option timestep="0.001" />

  <worldbody>
    <light pos="0 0 0" dir="0 0 -1" mode="fixed"/>
    <light pos="0 0 1" dir="0 0 -1" mode="fixed"/>
    <light pos="1 0 1" mode="fixed"/>
    <light pos="1 1 1" mode="fixed"/>
    <light pos="-1 1 1" mode="fixed"/>
    <geom size="5 5 .01" pos="-0.2 0 0" euler="0 90 0" type="plane" material="grid"/>
    <body name="sphere" pos="0 0 1">
        <freejoint/>
        <geom name="mouse_sphere" type="mesh" group="1" pos="0 0 4.5" quat="1 0 0 0" mesh="mesh_mouse_sphere" material="mouse_skin"/>
    </body>
  </worldbody>
</mujoco>
"""


def main():
    """ Main """
    # Read model
    model = mujoco.MjModel.from_xml_string(xml)
    # Get model data
    data = mujoco.MjData(model)
    # Simulate and display video.
    mujoco.mj_resetData(model, data)  # Reset state and time.
    # Load and visualize model
    viewer.launch(model)


if __name__ == '__main__':
    main()
