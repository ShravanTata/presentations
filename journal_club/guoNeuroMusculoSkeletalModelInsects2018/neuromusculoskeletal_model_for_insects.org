#+TITLE: A Neuro-Musculo-Skeletal Model for Insects With Data-driven Optimization
#+SUBTITLE: Shihui Guo, Juncong Lin, Toni Wöhrl, Minghong Liao
#+AUTHOR: Shravan Tata
#+EMAIL: Shravan.tataramalingasetty@epfl.ch
#+DATE: <2021-02-18 Thu 10:00>
#+STARTUP: beamer
#+DESCRIPTION: Dorosophila meeting presentation
#+LANGUAGE: en
#+OPTIONS: H:2 num:nil ^:{} toc:nil
#+LaTeX_CLASS_OPTIONS: [small, presentation, aspectratio=169]
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing,patterns}
#+LATEX_HEADER: \usetikzlibrary{positioning}
#+BIBILOGRAPHY: bibliography.bib
#+LATEX_HEADER: \usepackage[backend=bibtex]{biblatex}
#+LATEX_HEADER: \bibliography{bibliography.bib}
#+BEAMER_THEME: metropolis
#+BEAMER_COLOR_THEME: beaver
#+BEAMER_FONT_THEME: structuresmallcapsserif
#+BEAMER_HEADER: \graphicspath{{./media/}}
#+EXCLUDE_TAGS: noexport
#+PROPERTY:  header-args :eval no
* Introduction
** Overview
* Methods
** Neural control
** Muscular model
** Skeletal model
** Optimization
* Results and Discussion
** Optimization
** Validation
** Advantages of spiking neuron model
** Advantages of hill muscle model
* Thank you
