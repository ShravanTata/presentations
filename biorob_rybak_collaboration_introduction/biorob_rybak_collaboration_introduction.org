#+TITLE: BioRob - Laboratory for Theoretical & Computation Neuroscience
#+SUBTITLE: Collaboration introduction
#+BEAMER_HEADER: \title[Collaboration introduction]{BioRob - Laboratory for Theoretical \& Computation Neuroscience}
#+AUTHOR:    Shravan Tata
#+EMAIL:     Shravan.tataramalingasetty@epfl.ch
#+DATE:      <2020-06-24 Wed>
#+STARTUP: beamer
#+DESCRIPTION: Initiation meeting for collaboration between BioRob and Ilya Rybak's lab
#+LANGUAGE:  en
#+OPTIONS:   H:2 num:nil ^:{} toc:nil
#+LaTeX_CLASS_OPTIONS: [small, presentation, aspectratio=169]
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usepackage{media9}
#+LATEX_HEADER: \usetikzlibrary{decorations.pathmorphing,patterns}
#+LATEX_HEADER: \usetikzlibrary{positioning}
#+BIBILOGRAPHY: bibliography.bib
#+LATEX_HEADER: \usepackage[backend=bibtex]{biblatex}
#+LATEX_HEADER: \bibliography{bibliography.bib}
#+BEAMER_THEME: Madrid
#+BEAMER_COLOR_THEME: beaver
#+BEAMER_FONT_THEME: structuresmallcapsserif
#+BEAMER_HEADER: \graphicspath{{./figures/}}
#+EXCLUDE_TAGS: noexport
#+PROPERTY:  header-args :eval no

* Introduction
** Introduction
*** Map                                                               :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
     #+begin_center
     #+ATTR_LATEX: :scale 0.3  :center nil
     [[file:figures/bangalore.png]]
     #+end_center
*** Background                                                        :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - Origin : Bangalore, South of India
** Introduction
*** Map                                                               :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
     #+begin_center
     #+ATTR_LATEX: :scale 0.3  :center nil
     [[file:figures/bangalore-manipal.png]]
     #+end_center
*** Background                                                        :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - Origin : Bangalore, South of India
    - Bachelors Degree in Mechatronics, Manipal
** Introduction
*** Map                                                               :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
     #+begin_center
     #+ATTR_LATEX: :scale 0.3  :center nil
     [[file:figures/delft.png]]
     #+end_center
*** Background                                                        :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - Origin : Bangalore, South of India
    - Bachelor's Degree in Mechatronics, Manipal
    - Master's Degree in BioRobotics, Delft
** Introduction
*** Map                                                               :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
     #+begin_center
     #+ATTR_LATEX: :scale 0.15  :center nil
     [[file:figures/switzerland.png]]
     #+end_center
*** Background                                                        :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - Origin : Bangalore, South of India
    - Bachelor's Degree in Mechatronics, Manipal
    - Master's Degree in BioRobotics, Delft
    - PhD student at BioRob, Lausanne
* Research Interests
** Research
*** Outline                                                         :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:
    /“Use \alert{neuromuscular simulations} to study how the neural circuits in spinal cord interact with \alert{afferent feedback} and the \alert{descending modulation} to produce locomotion in terrestrial mammals”/
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
  #+begin_center
  #+ATTR_LATEX: :scale 0.25  :center nil
  [[file:figures/overview.png]]
  #+end_center
** Research - Examples
*** Animals                                                   :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - *Mouse*
*** Videos                                            :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
    - [[file:videos/cpg_spotlight_025x_documentation.mp4][Hindlimb demo]]
    - [[file:videos/mouse_quad_release.mp4][Quadruped demo with Danner et.al model video]]
    - [[file:videos/gait_result_1_2hz_stepping.mp4][Open loop Quadruped demo video]]
** Research - Examples
*** Animals                                                   :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - Mouse
    - *Rhesus monkey arm reaching*
*** Videos                                            :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
    - [[file:videos/cut_cropped.mp4][Monkey reaching experiment video]]
    - [[file:videos/FD.avi][Monkey reaching simulation video]]
** Research - Examples
*** Animals                                                   :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - Mouse
    - Rhesus monkey arm reaching
    - *Guinea fowl locomotion*
*** Videos                                            :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
    - [[file:videos/optimizedround2c.mp4][Guinea fowl open loop video]]
** Research - Examples
*** Animals                                                   :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :BEAMER_env: block
    :END:
    - Mouse
    - Rhesus monkey arm reaching
    - Guinea fowl locomotion
    - *Drosophilla locomotion*
*** Videos                                            :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:
    - [[file:videos/fly.mp4][Drosophila simulation pipeline demo]]
    - [[file:videos/KM_floor.mp4][Drosophila kinematic simulation demo]]
* Collaboration
** Collaboration
   - Work in progress collaboration with Simon Danner and Sergey Markin to study mice neural circuits for locomotion with sensory feedback
   #+BEAMER: \pause
   - Propose experiments with G-Lab (Courtine's group) to test and identify neural circuitry in mice with the help of simulations
   #+BEAMER: \pause
   - Setup cat simulation model to integrate the extensive knowledge base of cat studies from Rybak's team
   #+BEAMER: \pause
   - Explore role of descending modulation
* End
** Transition
   :PROPERTIES:
   :BEAMER_env: fullframe
   :END:
   #+begin_center
   \huge
   *Thank you \newline Questions*
   #+end_center
